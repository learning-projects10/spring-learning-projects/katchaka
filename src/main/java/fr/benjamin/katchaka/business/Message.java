package fr.benjamin.katchaka.business;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Message implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String contenu;
	
	private Date dateEnvoi;
	
	private Date dateLecture;
	
	@ManyToOne
	private Personne expediteur;
	
	@ManyToOne
	private Personne destinataire;
	
	@ManyToOne
	private VieCommune vieCommune;
	
	public Message() {
	}

	public Message(String contenu, Date dateEnvoi, Date dateLecture, Personne expediteur, Personne destinataire,
			VieCommune vieCommune) {
		super();
		this.contenu = contenu;
		this.dateEnvoi = dateEnvoi;
		this.dateLecture = dateLecture;
		this.expediteur = expediteur;
		this.destinataire = destinataire;
		this.vieCommune = vieCommune;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getContenu() {
		return contenu;
	}

	public void setContenu(String contenu) {
		this.contenu = contenu;
	}

	public Date getDateEnvoi() {
		return dateEnvoi;
	}

	public void setDateEnvoi(Date dateEnvoi) {
		this.dateEnvoi = dateEnvoi;
	}

	public Date getDateLecture() {
		return dateLecture;
	}

	public void setDateLecture(Date dateLecture) {
		this.dateLecture = dateLecture;
	}

	public Personne getExpediteur() {
		return expediteur;
	}

	public void setExpediteur(Personne expediteur) {
		this.expediteur = expediteur;
	}

	public Personne getDestinataire() {
		return destinataire;
	}

	public void setDestinataire(Personne destinataire) {
		this.destinataire = destinataire;
	}

	public VieCommune getVieCommune() {
		return vieCommune;
	}

	public void setVieCommune(VieCommune vieCommune) {
		this.vieCommune = vieCommune;
	}

	@Override
	public String toString() {
		return "Message [id=" + id + ", contenu=" + contenu + ", dateEnvoi=" + dateEnvoi
				+ ", dateLecture=" + dateLecture + ", expediteur=" + expediteur + ", destinataire=" + destinataire
				+ ", vieCommune=" + vieCommune + "]";
	}
	
	
}
