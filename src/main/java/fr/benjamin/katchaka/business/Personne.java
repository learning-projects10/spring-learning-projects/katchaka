package fr.benjamin.katchaka.business;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
public class Personne implements Serializable {
	private static final long serialVersionUID = 1L;

	public static int NB_CREDIT_INITIAL = 500;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(unique = true, nullable = false)
	@NotBlank(message="Merci de préciser une adresse mail.")
	@Email(message="L''adresse email renseignée n'est pas valide.")
	private String email;

	@Column(nullable = false)
	@Size(max=64)
	@Pattern(regexp="^(?=.{5,}$).*$", message="Le mot de passe doit être de 5 caractères minimum.")
	private String motDePasse;
	
	@Temporal(TemporalType.DATE)
	@Past(message="Votre date de naissance doit être dans le passé")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date dateDeNaissance;

	@Column(nullable = false)
	@NotBlank(message="Merci de préciser un pseudo.")
	@Size(max=50)
	@Pattern(regexp = "^[A-Za-z]+$", message = "Votre pseudo doit contenir uniquement des lettres")
	private String pseudo;

	@Size(min=20, max=1000, message="La bio doit contenir entre 20 et 1000 caractères.")
	private String bio;
	
	@Min(value=0, message="Les crédits ne peuvent pas être inférieur à zéro.")
	private int nCredits = NB_CREDIT_INITIAL;
	
	private boolean estFumeur;
	
	@NotNull
	@ManyToOne
	private Ville ville;
	
	@NotNull
	@ManyToOne
	private Statut statut;
	
	@NotNull
	@ManyToOne
	private Genre genre;
	
	@NotNull
	@ManyToOne
	private Genre genreRecherche;
	
	@NotNull
	@ManyToMany
	private List<Interet> interets;
	
	@OneToMany(mappedBy="expediteur")
	private List<Message> messageRecus;
	
	@OneToMany(mappedBy="destinataire")
	private List<Message> messageEnvoyes;
	
	@OneToMany(mappedBy="expediteur")
	private List<Invitation> invitationsRecues;
	
	@OneToMany(mappedBy="destinataire")
	private List<Invitation> invitationsEnvoyees;

	public Personne() {
	}

	public Personne(
			@NotBlank(message = "Merci de préciser une adresse mail.") @Email(message = "L''adresse email renseignée n'est pas valide.") String email,
			@Size(max = 64) @Pattern(regexp = "^(?=.{5,}$).*$", message = "Le mot de passe doit être de 5 caractères minimum.") String motDePasse,
			@Past(message = "Votre date de naissance doit être dans le passé") Date dateDeNaissance,
			@NotBlank(message = "Merci de préciser un pseudo.") @Size(max = 50) @Pattern(regexp = "^[A-Za-z]+$", message = "Votre pseudo doit contenir uniquement des lettres") String pseudo,
			@Size(min = 20, max = 1000, message = "La bio doit contenir entre 20 et 1000 caractères.") String bio,
			boolean estFumeur, @NotNull Ville ville, @NotNull Statut statut, @NotNull Genre genre,
			@NotNull Genre genreRecherche, @NotNull List<Interet> interets) {
		super();
		this.email = email;
		this.motDePasse = motDePasse;
		this.dateDeNaissance = dateDeNaissance;
		this.pseudo = pseudo;
		this.bio = bio;
		this.estFumeur = estFumeur;
		this.ville = ville;
		this.statut = statut;
		this.genre = genre;
		this.genreRecherche = genreRecherche;
		this.interets = interets;
		this.nCredits = NB_CREDIT_INITIAL;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMotDePasse() {
		return motDePasse;
	}

	public void setMotDePasse(String motDePasse) {
		this.motDePasse = motDePasse;
	}

	public Date getDateDeNaissance() {
		return dateDeNaissance;
	}

	public void setDateDeNaissance(Date dateDeNaissance) {
		this.dateDeNaissance = dateDeNaissance;
	}

	public String getPseudo() {
		return pseudo;
	}

	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}

	public String getBio() {
		return bio;
	}

	public void setBio(String bio) {
		this.bio = bio;
	}

	public int getnCredits() {
		return nCredits;
	}

	public void setnCredits(int nCredits) {
		this.nCredits = nCredits;
	}

	public boolean isEstFumeur() {
		return estFumeur;
	}

	public void setEstFumeur(boolean estFumeur) {
		this.estFumeur = estFumeur;
	}

	public Ville getVille() {
		return ville;
	}

	public void setVille(Ville ville) {
		this.ville = ville;
	}

	public Statut getStatut() {
		return statut;
	}

	public void setStatut(Statut statut) {
		this.statut = statut;
	}

	public Genre getGenre() {
		return genre;
	}

	public void setGenre(Genre genre) {
		this.genre = genre;
	}

	public Genre getGenreRecherche() {
		return genreRecherche;
	}

	public void setGenreRecherche(Genre genreRecherche) {
		this.genreRecherche = genreRecherche;
	}

	public List<Interet> getInterets() {
		return interets;
	}

	public void setInterets(List<Interet> interets) {
		this.interets = interets;
	}

	public List<Message> getMessageRecus() {
		return messageRecus;
	}

	public void setMessageRecus(List<Message> messageRecus) {
		this.messageRecus = messageRecus;
	}

	public List<Message> getMessageEnvoyes() {
		return messageEnvoyes;
	}

	public void setMessageEnvoyes(List<Message> messageEnvoyes) {
		this.messageEnvoyes = messageEnvoyes;
	}

	public List<Invitation> getInvitationsRecues() {
		return invitationsRecues;
	}

	public void setInvitationsRecues(List<Invitation> invitationsRecues) {
		this.invitationsRecues = invitationsRecues;
	}

	public List<Invitation> getInvitationsEnvoyees() {
		return invitationsEnvoyees;
	}

	public void setInvitationsEnvoyees(List<Invitation> invitationsEnvoyees) {
		this.invitationsEnvoyees = invitationsEnvoyees;
	}

	@Override
	public String toString() {
		return "Personne [id=" + id + ", email=" + email + ", motDePasse=" + motDePasse
				+ ", dateDeNaissance=" + dateDeNaissance + ", pseudo=" + pseudo + ", bio=" + bio + ", nCredits="
				+ nCredits + ", estFumeur=" + estFumeur + ", ville=" + ville + ", statut=" + statut + ", genre=" + genre
				+ ", genreRecherche=" + genreRecherche + "]";
	}

	
}