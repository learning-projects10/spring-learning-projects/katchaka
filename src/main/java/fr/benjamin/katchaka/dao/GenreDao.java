package fr.benjamin.katchaka.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.benjamin.katchaka.business.Genre;

public interface GenreDao extends JpaRepository<Genre, Long> {
}
