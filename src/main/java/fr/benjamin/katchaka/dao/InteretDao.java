package fr.benjamin.katchaka.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.benjamin.katchaka.business.Interet;

public interface InteretDao extends JpaRepository<Interet, Long> {

}
