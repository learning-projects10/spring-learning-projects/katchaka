package fr.benjamin.katchaka.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.benjamin.katchaka.business.Statut;

public interface StatutDao extends JpaRepository<Statut, Long> {

}
