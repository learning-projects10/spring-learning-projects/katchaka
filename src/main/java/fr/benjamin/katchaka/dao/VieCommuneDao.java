package fr.benjamin.katchaka.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.benjamin.katchaka.business.VieCommune;

public interface VieCommuneDao extends JpaRepository<VieCommune, Long> {

}
