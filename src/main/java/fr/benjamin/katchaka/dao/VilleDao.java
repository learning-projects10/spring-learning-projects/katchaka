package fr.benjamin.katchaka.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.benjamin.katchaka.business.Ville;

public interface VilleDao extends JpaRepository<Ville, Long> {

}
