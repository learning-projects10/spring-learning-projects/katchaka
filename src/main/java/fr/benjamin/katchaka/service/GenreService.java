package fr.benjamin.katchaka.service;

import java.util.List;
import java.util.Optional;

import fr.benjamin.katchaka.business.Genre;

public interface GenreService {

	Genre creerGenre(Genre genre);
	
	Optional<Genre> recupererGenre(Long idGenre);
	
	List<Genre> recupererGenres();
	
	Genre majGenre(Genre genre);

	boolean supprimerGenre(Long idGenre);

}
