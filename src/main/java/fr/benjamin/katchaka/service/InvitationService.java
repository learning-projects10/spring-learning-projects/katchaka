package fr.benjamin.katchaka.service;

import java.util.List;
import java.util.Optional;

import fr.benjamin.katchaka.business.Invitation;
import fr.benjamin.katchaka.business.Personne;

public interface InvitationService {

	Invitation creerInvitation(Invitation invitation);
	
	Optional<Invitation> recupererInvitation(Long idInvitation);
	
	List<Invitation> recupererInvitations();
	
	List<Invitation> recupererInvitationsByDestinataire(Personne destinataire);
	
	List<Invitation> recupererInvitationsByExpediteur(Personne expediteur);
	
	Invitation majInvitation(Invitation invitation);
	
	boolean supprimerInvitation(Long idInvitation);
}
