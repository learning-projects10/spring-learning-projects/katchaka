package fr.benjamin.katchaka.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import fr.benjamin.katchaka.business.Genre;
import fr.benjamin.katchaka.business.Interet;
import fr.benjamin.katchaka.business.Personne;
import fr.benjamin.katchaka.business.Ville;

public interface PersonneService {

	Personne creerPersonne(Personne personne);
	
	Optional<Personne> recupererPersonne(Long idPersonne);
	
	Personne recupererPersonneByEmailAndMotDePasse(String adresseMail, String motDePasse);
	
	List<Personne> recupererPersonnes();
	
	Page<Personne> recupererPersonnes(Pageable pageable, Ville villeRecherchee, Interet interetRecherche,
			Genre genreRecherche);
	
	Personne majPersonne(Personne personne);
	
	boolean supprimerPersonne(Long idPersonne);
}
