package fr.benjamin.katchaka.service;

import java.util.List;
import java.util.Optional;

import fr.benjamin.katchaka.business.Statut;

public interface StatutService {

	Statut creerStatut(Statut statut);
	
	Optional<Statut> recupererStatut(Long idStatut);
	
	List<Statut> recupererStatuts();
	
	Statut majStatut(Statut statut);
	
	boolean supprimerStatut(Long idStatut);
}
