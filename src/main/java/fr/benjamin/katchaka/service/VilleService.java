package fr.benjamin.katchaka.service;

import java.util.List;
import java.util.Optional;

import fr.benjamin.katchaka.business.Ville;

public interface VilleService {

	Ville creerVille(Ville ville);
	
	Optional<Ville> recupererVille(Long idVille);
	
	List<Ville> recupererVilles();
	
	Ville majVille(Ville ville);
	
	boolean supprimerVille(Long idVille);
}
