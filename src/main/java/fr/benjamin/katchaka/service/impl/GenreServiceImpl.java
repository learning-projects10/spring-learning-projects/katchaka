package fr.benjamin.katchaka.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.benjamin.katchaka.business.Genre;
import fr.benjamin.katchaka.dao.GenreDao;
import fr.benjamin.katchaka.service.GenreService;

@Service
@Transactional
public class GenreServiceImpl implements GenreService {

	@Autowired
	private GenreDao genreDao;
	
	@Override
	public Genre creerGenre(Genre genre) {
		return genreDao.save(genre);
	}

	@Override
	public Optional<Genre> recupererGenre(Long idGenre) {
		return genreDao.findById(idGenre);
	}

	@Override
	public List<Genre> recupererGenres() {
		return genreDao.findAll();
	}

	@Override
	public Genre majGenre(Genre genre) {
		return genreDao.save(genre);
	}

	@Override
	public boolean supprimerGenre(Long idGenre) {
		Genre genre = recupererGenre(idGenre).get();
		if (genre == null) {
			return false;
		} else {
			genreDao.deleteById(idGenre);
			return true;
		}
	}

}
