package fr.benjamin.katchaka.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.benjamin.katchaka.business.Interet;
import fr.benjamin.katchaka.dao.InteretDao;
import fr.benjamin.katchaka.service.InteretService;

@Service
public class InteretServiceImpl implements InteretService {

	@Autowired
	private InteretDao interetDao;
	
	@Override
	public Interet creerInteret(Interet interet) {
		return interetDao.save(interet);
	}

	@Override
	public Optional<Interet> recupererInteret(Long idInteret) {
		return interetDao.findById(idInteret);
	}

	@Override
	public List<Interet> recupererInterets() {
		return interetDao.findAll();
	}

	@Override
	public Interet majInteret(Interet interet) {
		return interetDao.save(interet);
	}

	@Override
	public boolean supprimerInteret(Long idInteret) {
		Interet interet = recupererInteret(idInteret).get();
		if (interet == null) {
			return false;
		} else {
			interetDao.deleteById(idInteret);
			return true;
		}
	}

}
