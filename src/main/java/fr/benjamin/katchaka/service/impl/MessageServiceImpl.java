package fr.benjamin.katchaka.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.benjamin.katchaka.business.Message;
import fr.benjamin.katchaka.dao.MessageDao;
import fr.benjamin.katchaka.service.MessageService;

@Service
public class MessageServiceImpl implements MessageService {

	@Autowired
	private MessageDao messageDao;
	
	@Override
	public Message creerMessage(Message message) {
		return messageDao.save(message);
	}

	@Override
	public Optional<Message> recupererMessage(Long idMessage) {
		return messageDao.findById(idMessage);
	}

	@Override
	public List<Message> recupererMessages() {
		return messageDao.findAll();
	}

	@Override
	public Message majMessage(Message message) {
		return messageDao.save(message);
	}

	@Override
	public boolean supprimerMessage(Long idMessage) {
		Message message = recupererMessage(idMessage).get();
		if (message == null) {
			return false;
		} else {
			messageDao.deleteById(idMessage);
			return true;
		}
	}

}
