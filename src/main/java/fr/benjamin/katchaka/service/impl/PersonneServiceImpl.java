package fr.benjamin.katchaka.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import fr.benjamin.katchaka.business.Genre;
import fr.benjamin.katchaka.business.Interet;
import fr.benjamin.katchaka.business.Personne;
import fr.benjamin.katchaka.business.Ville;
import fr.benjamin.katchaka.dao.PersonneDao;
import fr.benjamin.katchaka.service.PersonneService;

@Service
public class PersonneServiceImpl implements PersonneService {

	@Autowired
	private PersonneDao personneDao;
	
	@Override
	public Personne creerPersonne(Personne personne) {
		return personneDao.save(personne);
	}

	@Override
	public Optional<Personne> recupererPersonne(Long idPersonne) {
		return personneDao.findById(idPersonne);
	}
	
	@Override
	public Personne recupererPersonneByEmailAndMotDePasse(String email, String motDePasse) {
		return personneDao.findByEmailAndMotDePasse(email, motDePasse);
	}

	@Override
	public List<Personne> recupererPersonnes() {
		return personneDao.findAll();
	}
	
	@Override
	public Page<Personne> recupererPersonnes(Pageable pageable, Ville villeRecherchee, Interet interetRecherche,
			Genre genreRecherche) {
		if (villeRecherchee == null && interetRecherche == null) {
			return personneDao.findAllByGenre(pageable, genreRecherche);
		} else if (villeRecherchee != null && interetRecherche == null) {
			return personneDao.findAllByVilleAndGenre(pageable, villeRecherchee, genreRecherche);
		} else if (villeRecherchee == null && interetRecherche != null) {
			return personneDao.findAllByInteretsAndGenre(pageable, interetRecherche, genreRecherche);
		} else if (villeRecherchee != null && genreRecherche != null) {
			return personneDao.findAllByVilleAndInteretsAndGenre(pageable, villeRecherchee, interetRecherche, genreRecherche);
		}

		return null;
	}

	@Override
	public Personne majPersonne(Personne personne) {
		return personneDao.save(personne);
	}

	@Override
	public boolean supprimerPersonne(Long idPersonne) {
		Personne personne = recupererPersonne(idPersonne).get();
		if (personne == null) {
			return false;
		} else {
			personneDao.deleteById(idPersonne);
			return true;
		}
	}

}
