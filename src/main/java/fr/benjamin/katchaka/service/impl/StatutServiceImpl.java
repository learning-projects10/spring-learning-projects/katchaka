package fr.benjamin.katchaka.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.benjamin.katchaka.business.Statut;
import fr.benjamin.katchaka.dao.StatutDao;
import fr.benjamin.katchaka.service.StatutService;

@Service
public class StatutServiceImpl implements StatutService {

	@Autowired
	private StatutDao statutDao;
	
	@Override
	public Statut creerStatut(Statut statut) {
		return statutDao.save(statut);
	}

	@Override
	public Optional<Statut> recupererStatut(Long idStatut) {
		return statutDao.findById(idStatut);
	}

	@Override
	public List<Statut> recupererStatuts() {
		return statutDao.findAll();
	}

	@Override
	public Statut majStatut(Statut statut) {
		return statutDao.save(statut);
	}

	@Override
	public boolean supprimerStatut(Long idStatut) {
		Statut statut = recupererStatut(idStatut).get();
		if (statut == null) {
			return false;
		} else {
			statutDao.deleteById(idStatut);
			return true;
		}
	}

}
