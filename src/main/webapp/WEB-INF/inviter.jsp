<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Profil</title>
	    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
	    <link rel="stylesheet" href="css/style.css">
	</head>
	<body class="container my-3">
	
		<jsp:include page="componants/header.jsp"></jsp:include>
			
		<form action="inviter?destinataire=${profil.id}" method="post">
			<div class="form-group">
				<h2>Inviter ${profil.pseudo}</h2>
			</div>
			<div class="form-group">
				<label for="message">Message : </label>
				<textarea class="form-control" name="MESSAGE" id="message" rows="10" cols="50"></textarea>
			</div>
			<input class="btn btn-success btn-sm my-2" type="submit" value="Inviter">
		</form>
		
		<a href="accueil" class="btn btn-primary btn-sm">Liste des personnes</a>
		<a href="liste-invitation" class="btn btn-primary btn-sm">Liste des invitations</a>
	</body>
</html>