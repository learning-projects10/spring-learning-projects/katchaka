<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Liste des inviations</title>
	    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
	    <link rel="stylesheet" href="css/style.css">
	</head>
	<body class="container my-3">
		<main>

			<jsp:include page="componants/header.jsp"></jsp:include>
			
			<section>
				<h3>Liste des invitations reçues</h3>
				
				<table class="table table-striped table-bordered">
					<thead>
						<tr>
							<th>Pseudo</th>
							<th>Ville</th>
							<th>Envoyée le</th>
							<th>Opérations</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="invitationRecue" items="${listeInvitationRecues}">
							<tr>
								<td class="align-middle"><a href="profil?ID=${invitationRecue.expediteur.id}">${invitationRecue.expediteur.pseudo}</a></td>
								<td class="align-middle">${invitationRecue.expediteur.ville.nom}</td>
								<td class="align-middle">${invitationRecue.dateEnvoi}</td>
								<td class="align-middle">
									<c:choose>
										<c:when test="${invitationRecue.estAcceptee eq null}">
											<a href="accepterInvitation?ID=${invitationRecue.id}" class="btn btn-primary btn-sm">Accepter</a> 
											<a href="declinerInvitation?ID=${invitationRecue.id}" class="btn btn-primary btn-sm">Décliner</a>
										</c:when>
										<c:when test="${invitationRecue.estAcceptee eq true}">
											Acceptée le ${invitationRecue.dateLecture}
										</c:when>
										<c:when test="${invitationRecue.estAcceptee eq false}">
											Décliné le ${invitationRecue.dateLecture}
										</c:when>
									</c:choose>
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</section>
			
			<section>
				<h3>Liste des invitations envoyées</h3>
				
				<table class="table table-striped table-bordered">
					<thead>
						<tr>
							<th>Pseudo</th>
							<th>Statut</th>
							<th>Envoyée le</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="invitationEnvoyee" items="${listeInvitationEnvoyees}">
							<tr>
								<td class="align-middle">${invitationEnvoyee.destinataire.pseudo}</td>
								<td class="align-middle">${invitationEnvoyee.destinataire.statut.nom}</td>
								<td class="align-middle">${invitationEnvoyee.dateEnvoi}</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</section>
			
			<a href="index" class="btn btn-primary btn-sm">Liste des personnes</a>
		</main>
	</body>
</html>