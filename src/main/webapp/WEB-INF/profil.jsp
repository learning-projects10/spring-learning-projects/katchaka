<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Profil</title>
	    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
	    <link rel="stylesheet" href="css/style.css">
	</head>
	<body class="container my-3">
	
		<jsp:include page="componants/header.jsp"></jsp:include>
			
		<div class="card my-3">
			<div class="card-header">
			 	<h2>Profil de ${profil.pseudo}</h2>
			</div>
			<div class="card-body">
				<p class="card-text">Statut : ${profil.statut.nom}</p>
				
				<p class="card-text">Ville : ${profil.ville.nom}</p>
				
				<p class="card-text">Date de naissance : ${profil.dateDeNaissance}</p>
				
				<p class="card-text">Bio : ${profil.bio}</p>
		  	</div>
		</div>
		
		<a href="accueil" class="btn btn-primary btn-sm">Liste des personnes</a>
		<a href="liste-invitation" class="btn btn-primary btn-sm">Liste des invitations</a>
	</body>
</html>